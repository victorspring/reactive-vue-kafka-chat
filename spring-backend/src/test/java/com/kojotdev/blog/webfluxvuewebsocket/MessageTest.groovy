package com.kojotdev.blog.webfluxvuewebsocket

import com.kojotdev.blog.webfluxvuewebsocket.data.Message
import com.kojotdev.blog.webfluxvuewebsocket.data.HelloMessage
import spock.lang.Specification

class MessageTest extends Specification {

    def "Greeting from HelloMessage should be correct"() {
        given: "simple HelloMessage"
        def helloMessage = new HelloMessage("kojot")
        when: "creating greetings from HelloMessage"
        def greeting = Message.from(helloMessage, instanceId)
        then: "greeting should say hello with name!"
        greeting.content == "Hello, kojot!"
    }
}
