package com.kojotdev.blog.webfluxvuewebsocket.data;

public class Message {

    public String content;

    public Message() {
    }

    public Message(String content) {
        this.content = content;
    }

    public static Message from(HelloMessage helloMessage, Long instanceId) {
        return new Message(helloMessage.getName() + " from instance " + instanceId);
    }
}
