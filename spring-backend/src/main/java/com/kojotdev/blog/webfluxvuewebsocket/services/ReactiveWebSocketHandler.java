package com.kojotdev.blog.webfluxvuewebsocket.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kojotdev.blog.webfluxvuewebsocket.data.Message;
import com.kojotdev.blog.webfluxvuewebsocket.data.HelloMessage;
//import com.kojotdev.blog.webfluxvuewebsocket.repository.MessageRepository;
import io.vavr.control.Try;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.receiver.internals.ConsumerFactory;
import reactor.kafka.receiver.internals.DefaultKafkaReceiver;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderOptions;
import reactor.kafka.sender.SenderRecord;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;



@Component
public class ReactiveWebSocketHandler implements WebSocketHandler {

    public static final String GROUP_ID_CONFIG = "kafka-sandbox";

    private static final String TOPIC = "kafka-chat";
    private static final String BOOTSTRAP_SERVERS = "localhost:9092";
    private static final String CLIENT_ID_CONFIG = "trans-string-consumer-egen-new";

    private final KafkaSender<String, String> sender;


    private ObjectMapper jsonMapper = new ObjectMapper();

    @Value("${eureka.instance.instance-id}")
    private Long instanceId;

    @Autowired
    public ReactiveWebSocketHandler() {

        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, CLIENT_ID_CONFIG);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        SenderOptions<String, String> senderOptions = SenderOptions.create(props);
        sender = KafkaSender.create(senderOptions);
    }

    @Override
    public Mono<Void> handle(WebSocketSession webSocketSession) {
        return webSocketSession.send(kafkaReceiver(webSocketSession.getId())
                        .receive()
                        .map(t -> webSocketSession.textMessage(t.value())))
                .and(sender.send(webSocketSession.receive()
                        .map(WebSocketMessage::getPayloadAsText)
                        .map(message -> Try.of(() -> jsonMapper.readValue(message, HelloMessage.class)).get())
                        .map(message -> Try.of(() -> jsonMapper.writeValueAsString(Message.from(message, instanceId))).get())
                        .map(i -> SenderRecord.create(new ProducerRecord<>(TOPIC, "Key_" + i, i), i))));

    }

    public KafkaReceiver<String, String> kafkaReceiver(String clientID){

        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID_CONFIG + "-" + clientID);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);

        return new DefaultKafkaReceiver(ConsumerFactory.INSTANCE, ReceiverOptions.create(props).subscription(Collections.singleton(TOPIC)));
    }
}
